import { SwitchNavigator } from 'react-navigation';
import { OnBoardingStack } from './stackNavigator';
import { AppStack } from './tabNavigator';

export default SwitchNavigator(
  {
    OnBoarding: OnBoardingStack,
    App: AppStack
  },
  {
    initialRouteName: 'OnBoarding'
  }
);
