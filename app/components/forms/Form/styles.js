import { StyleSheet } from 'react-native';

import {
  FONT_ERROR,
  COLOR_GREY,
  COLOR_GREY_EXTRA_STRONG,
  FONT_DISPLAY_MEDIUM,
  FONT_DROPDOWN_REGULAR,
  COLOR_WHITE
} from '../../../styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_WHITE
  },
  credentialsContainer: {
    justifyContent: 'space-between'
  },
  rememberMe: {
    alignSelf: 'flex-start',
    marginTop: 15
  },
  buttonContainer: {
    marginTop: 56
  },
  errorStyle: {
    marginLeft: 30,
    ...FONT_ERROR,
    width: 248
  },
  logo: {
    width: 150,
    height: 106,
    marginTop: 63
  },
  fieldContainer: {
    marginTop: 12,
    width: 278
  },
  title: {
    ...FONT_DISPLAY_MEDIUM,
    color: COLOR_GREY
  },
  dobButton: {
    marginTop: 0,
    alignItems: 'center',
    borderRadius: 4,
    borderColor: COLOR_GREY,
    borderWidth: 1,
    width: 153,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 3
  },
  dobText: {
    ...FONT_DROPDOWN_REGULAR,
    paddingVertical: 8,
    color: COLOR_GREY_EXTRA_STRONG
  },
  arrow: {
    height: 16,
    width: 16
  },
  radioContainer: {
    width: '100%',
    marginTop: 8,
    justifyContent: 'space-between'
  },
  customContainerStyle: {
    alignSelf: 'center',
    marginTop: 25
  }
});
