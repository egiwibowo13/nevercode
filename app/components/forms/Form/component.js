import React from 'react';
import {
  KeyboardAvoidingView,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform
} from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Dropdown from '../../elements/Dropdown';
import FieldInput from '../../elements/FieldInput';
import PrimaryButton from '../../elements/PrimaryButton';
import RadioGroup from '../../elements/RadioGroup';
import CheckBox from '../../elements/CheckBox';
import { IMAGES } from '../../../configs';
import styles from './styles';
import ModalDatePicker from '../../elements/ModalDatePicker';

import { noop, formatDate } from '../../../utils';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loginError: false,
      isModal: false
    };
  }

  _onChangeText = key => value => {
    const { onFormChange = noop } = this.props;
    onFormChange(key, value);
  };

  _onItemSelected = key => value => {
    const { onFormChange = noop } = this.props;
    onFormChange(key, value.name);
  };

  _onOpenDatePicker = (key, defaultValue) => async () => {
    await this.setState({
      isModal: true,
      keyPicker: key,
      defaultValueDate: defaultValue
    });
  };

  _onDateChange = async newDate => {
    if (Platform.OS === 'android') {
      await this.setState({
        isModal: false
      });
    }
    await this.setState({
      defaultValueDate: newDate
    });
  };

  _onCloseModalDatePicker = async () => {
    const { keyPicker, defaultValueDate } = this.state;
    const { onFormChange = noop } = this.props;
    await this.setState({
      isModal: false
    });
    await onFormChange(keyPicker, defaultValueDate);
  };

  _onRememberMeChanged = key => async value => {
    const { onFormChange = noop } = this.props;
    onFormChange(key, value);
  };

  _onSelectRadioButton = key => async (index, selectingName) => {
    const { onFormChange = noop } = this.props;
    onFormChange(key, selectingName);
  };

  _renderButton = (item, idx) => {
    const { onPress = noop, disabled, label, customContainerItemStyle = {} } = item;
    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <PrimaryButton
          customContainerStyle={styles.customContainerStyle}
          disabled={disabled}
          onPress={onPress}
          title={label}
        />
      </View>
    );
  };

  _renderCheckBox = (item, idx) => {
    const { defaultValue, label, key, customContainerItemStyle = {} } = item;
    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <CheckBox
          style={styles.rememberMe}
          value={defaultValue}
          text={label}
          onValueChange={this._onRememberMeChanged(key)}
        />
      </View>
    );
  };

  _renderDatePicker = (item, idx) => {
    const {
      label = 'Date : ',
      placeholder = 'dd/mm/yy',
      defaultValue,
      key,
      customContainerItemStyle = {}
    } = item;
    let isDate;

    if (Platform.OS === 'ios') {
      isDate = _.isDate(defaultValue);
    } else {
      isDate = !_.isEmpty(defaultValue);
    }

    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <Text style={styles.title}>{label}</Text>
        <TouchableOpacity style={styles.dobButton} onPress={this._onOpenDatePicker(key, defaultValue)}>
          {isDate && <Text style={styles.dobText}>{formatDate(defaultValue, 'DD/MM/YYYY')}</Text>}
          {!isDate && <Text style={styles.dobText}>{placeholder}</Text>}
          <Image source={IMAGES.dropdown} style={styles.arrow} />
        </TouchableOpacity>
      </View>
    );
  };

  _renderDropDown = (item, idx) => {
    const {
      options = [],
      label = '',
      placeholder = 'Pilih',
      defaultValue,
      key,
      customContainerItemStyle = {}
    } = item;
    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <Text style={{ marginBottom: 20 }}>{label}</Text>
        <Dropdown
          options={options}
          onItemSelected={this._onItemSelected(key)}
          placeholder={placeholder}
          defaultId={defaultValue}
        />
      </View>
    );
  };

  _renderErrorLabel() {
    const { loginError } = this.state;
    return loginError ? <Text style={styles.errorStyle}>error</Text> : null;
  }

  _renderInput = (item, idx) => {
    const {
      label = '',
      placeholder = '',
      defaultValue,
      key = 'text',
      images,
      validate,
      errorText,
      floatingLabel,
      onFocus,
      customContainerItemStyle = {},
      secureTextEntry
    } = item;
    const highlightedIcon = _.has(images, 'on') && images.on;
    const normalIcon = _.has(images, 'off') && images.off;
    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <FieldInput
          floatingLabel={floatingLabel}
          label={label}
          placeholder={placeholder}
          defaultValue={defaultValue}
          onChangeInput={this._onChangeText(key)}
          onFocus={onFocus}
          validate={validate}
          errorText={errorText}
          textInputProps={{
            iconEnabled: true,
            highlightedIcon,
            normalIcon,
            secureTextEntry
          }}
        />
      </View>
    );
  };

  _renderRadioButton = (item, idx) => {
    const { defaultValue, options = [], key, label, customContainerItemStyle = {} } = item;
    return (
      <View key={idx} style={[styles.fieldContainer, customContainerItemStyle]}>
        <Text style={styles.title}>{label}</Text>
        <View style={styles.radioContainer}>
          <RadioGroup
            options={options}
            spaceBetween={0}
            selected={defaultValue}
            onSelect={this._onSelectRadioButton(key)}
          />
        </View>
      </View>
    );
  };

  render() {
    const { data = [] } = this.props;
    const { isModal, defaultValueDate } = this.state;
    return (
      <View onLayout={this._onLayout}>
        <ScrollView scrollEnabled={!isModal}>
          <KeyboardAvoidingView style={styles.container}>
            {data.map((item, idx) => {
              if (item.type === 'TextIntup') {
                return this._renderInput(item, idx);
              } else if (item.type === 'Picker') {
                return this._renderDatePicker(item, idx);
              } else if (item.type === 'DropDown') {
                return this._renderDropDown(item, idx);
              } else if (item.type === 'RadioButton') {
                return this._renderRadioButton(item, idx);
              } else if (item.type === 'Button') {
                return this._renderButton(item, idx);
              } else if (item.type === 'CheckBox') {
                return this._renderCheckBox(item, idx);
              } else if (item.type === 'children') {
                return item.children;
              }
              return this._renderInput(item, idx);
            })}
            {this._renderErrorLabel()}
          </KeyboardAvoidingView>
        </ScrollView>
        <ModalDatePicker
          visible={isModal}
          date={defaultValueDate}
          onDateChange={this._onDateChange}
          onCloseModal={this._onCloseModalDatePicker}
        />
      </View>
    );
  }
}

Component.propTypes = {
  onFormChange: PropTypes.func.isRequired,
  data: [
    {
      // type is required ("TextInput","Picker","DropDown","RadioButton","Button","CheckBox","children")
      type: PropTypes.string.isRequired, // All
      defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired, // All
      key: PropTypes.string.isRequired, // All
      label: PropTypes.string, // All
      customContainerItemStyle: PropTypes.object, // All
      placeholder: PropTypes.string, // Picker, DropDown, TextInput
      errorText: PropTypes.string, // TextInput
      floatingLabel: PropTypes.bool, // TextInput
      validate: PropTypes.func, // TextInput
      secureTextEntry: PropTypes.string, // TextInput
      onFocus: PropTypes.func, // TextInput
      images: {
        // TextInput
        on: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
        off: PropTypes.oneOfType([PropTypes.object, PropTypes.number])
      },
      options: [
        // RadioButton, DropDown
        {
          id: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired
        }
      ],
      disabled: PropTypes.bool // Button
    }
  ]
};

Component.defaultProps = {
  data: [
    {
      customContainerItemStyle: {},
      label: '',
      placeholder: '',
      errorText: '',
      floatingLabel: false,
      validate: noop,
      secureTextEntry: 'default',
      onFocus: noop,
      images: {
        on: {},
        off: {}
      }
    }
  ]
};
