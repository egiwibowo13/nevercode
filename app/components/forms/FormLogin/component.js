import React from 'react';
import { View, Image, ScrollView, Alert } from 'react-native';
import _ from 'lodash';
import styles from './styles';
import Form from '../Form';
import { IMAGES, ENDPOINT } from '../../../configs';
import { isEmpty, isEmail } from '../../../utils';
import storage from '../../../utils/storage';
import { requiredField, validateEmail } from '../../../utils/validation';
import I18n from '../../../i18n';

const REMEMBER_ME = 'REMEMBER_ME';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorTextEmail: '',
      errorTextPassword: '',
      shouldRememberMe: false
    };
  }

  componentDidMount = async () => {
    const result = await storage.get(REMEMBER_ME);
    if (_.has(result, 'email')) this.setState({ email: result.email, shouldRememberMe: true });
    if (_.has(result, 'password')) this.setState({ password: result.password });
  };

  _isInvalidToSubmit = () => {
    const { email, password } = this.state;
    const result = isEmpty(email) || !isEmail(email) || isEmpty(password);
    return result;
  };

  _onFocus = () => {
    this.setState({
      errorTextEmail: '',
      errorTextPassword: ''
    });
  };

  _onPress = async () => {
    const { email, password } = this.state;
    const { navigation } = this.props;
    const params = {
      email,
      password
    };
    try {
      await this._saveToStorageRememberMe();
      const result = await ENDPOINT.login(params);
      if (_.has(result, 'error')) {
        this.setState({ errorTextPassword: result.error });
      } else {
        navigation.navigate('Home');
      }
    } catch (error) {
      Alert.alert(error);
    }
  };

  _onFormChange = async (key, value) => {
    await this.setState({ [key]: value });
    await this.setState({
      errorTextEmail: '',
      errorTextPassword: ''
    });
  };

  _saveToStorageRememberMe = () => {
    const { email, password, shouldRememberMe } = this.state;
    const params = {
      email,
      password
    };
    if (shouldRememberMe) {
      storage.set(REMEMBER_ME, params);
    } else {
      storage.remove(REMEMBER_ME);
    }
  };

  render() {
    const { email, password, errorTextEmail, errorTextPassword, shouldRememberMe } = this.state;
    const data = [
      {
        label: I18n.t('email'),
        floatingLabel: true,
        placeholder: I18n.t('email'),
        defaultValue: email,
        type: 'TextInput',
        key: 'email',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        },
        validate: isEmpty(email) ? requiredField : validateEmail,
        errorText: errorTextEmail,
        onFocus: this._onFocus
      },
      {
        label: I18n.t('password'),
        floatingLabel: true,
        placeholder: I18n.t('password'),
        defaultValue: password,
        type: 'TextInput',
        key: 'password',
        images: {
          on: IMAGES.passOn,
          off: IMAGES.passOff
        },
        secureTextEntry: true,
        validate: requiredField,
        errorText: errorTextPassword,
        onFocus: this._onFocus
      },
      {
        label: I18n.t('rememberMe'),
        type: 'CheckBox',
        defaultValue: shouldRememberMe,
        key: 'shouldRememberMe'
      },
      {
        label: I18n.t('submit'),
        type: 'Button',
        onPress: this._onPress,
        disabled: this._isInvalidToSubmit()
      }
    ];

    return (
      <ScrollView>
        <View style={styles.container}>
          <Image source={IMAGES.login} style={styles.image} />
          <Form data={data} onFormChange={this._onFormChange} />
        </View>
      </ScrollView>
    );
  }
}
