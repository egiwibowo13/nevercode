import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import Component from './component';
import validate from './validate';

function mapStateToProps(state) {
  const { createArticle } = state.form;
  const { values = {} } = createArticle;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {};
}

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);

export default reduxForm({
  form: 'createArticle',
  validate
})(Connected);
