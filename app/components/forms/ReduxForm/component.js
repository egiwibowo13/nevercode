import React from 'react';
import { Field } from 'redux-form';
import { View, Button, Alert } from 'react-native';
import PropTypes from 'prop-types';
import TextInputRedux from '../../elements/TextInputRedux';

export default class Component extends React.Component {
  _renderTextField(label, name) {
    return <Field component={TextInputRedux} label={label} name={name} />;
  }

  myBtn = values => {
    Alert.alert(values);
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <View>
        {this._renderTextField('First Name', 'fname')}
        {this._renderTextField('Last Name', 'lname')}
        <Button title="Submit" onPress={handleSubmit} />
      </View>
    );
  }
}

Component.propTypes = {
  handleSubmit: PropTypes.func.isRequired
};
