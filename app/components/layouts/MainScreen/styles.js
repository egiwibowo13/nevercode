import { StyleSheet } from 'react-native';
import { PRIMARY_COLOR } from '../../../styles';

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: PRIMARY_COLOR
  },
  container: {
    flex: 1
  },
  text: {
    fontSize: 20
  }
});

export default styles;
