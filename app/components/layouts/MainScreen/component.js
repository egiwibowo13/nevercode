import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Header from '../../elements/Header';
import Loading from '../../elements/Loading';

export default class Component extends React.Component {
  _onPressBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  render() {
    const { children, title, showSearchButton, isLoading } = this.props;
    return (
      <View style={styles.container}>
        {isLoading && <Loading />}
        <Header title={title} showSearchButton={showSearchButton} onPressBack={this._onPressBack} />
        {children}
      </View>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  title: PropTypes.string,
  showSearchButton: PropTypes.bool,
  isLoading: PropTypes.bool
};

Component.defaultProps = {
  title: '',
  showSearchButton: false,
  isLoading: true
};
