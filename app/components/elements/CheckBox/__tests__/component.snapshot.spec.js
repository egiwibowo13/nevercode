import { assertSnapshots } from '../../../../utils/TestUtils/snapshot';
import Component from '../index';

describe('Component snapshot testing', () => {
  const configs = [
    {
      state: {},
      props: {},
      desc: 'renders with default props'
    },
    {
      state: { isChecked: true },
      props: { text: 'text' },
      desc: 'renders with props'
    }
  ];
  assertSnapshots(Component, configs);
});
