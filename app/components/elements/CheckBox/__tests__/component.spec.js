import Component from '../index';
import { mockSetState } from '../../../../utils/TestUtils/snapshot';
import { noop } from '../../../../utils';

jest.mock('../styles.js', () => ({}));
jest.mock('View', () => 'View');
jest.mock('TouchableHighlight', () => 'TouchableHighlight');
jest.mock('Image', () => 'Image');
jest.mock('../../../../utils', () => ({
  noop: jest.fn()
}));

describe('Component', () => {
  const ComponentWithMockedState = mockSetState(Component);
  const getInstance = props => new ComponentWithMockedState(props);
  beforeEach(() => {
    this.props = {
      onValueChange: jest.fn()
    };
  });

  afterEach(() => {
    this.props = null;
    jest.clearAllMocks();
  });

  test('componentWillReceiveProps', () => {
    const instance = getInstance(this.props);
    const value = true;
    instance.componentWillReceiveProps(value);
    expect(instance.state.isChecked).toBeFalsy();
  });

  describe('_onPress', () => {
    test('_onPress onValueChange have value', () => {
      const instance = getInstance(this.props);
      const isChecked = true;
      instance.setState({
        isChecked
      });
      instance._onPress();
      expect(this.props.onValueChange).toHaveBeenCalledWith(!isChecked);
    });

    test('_onPress onValueChange undefined', () => {
      this.props.onValueChange = undefined;
      const instance = getInstance(this.props);
      const isClicked = true;
      instance.setState({
        isClicked
      });
      instance._onPress();
      expect(noop).toHaveBeenCalled();
    });
  });
});
