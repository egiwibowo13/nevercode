import React from 'react';
import { Image, View, TouchableHighlight, Text } from 'react-native';
import PropTypes from 'prop-types';
import { IMAGES } from '../../../configs';
import styles from './styles';
import { noop } from '../../../utils';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: this.props.value || false
    };
  }

  componentWillReceiveProps({ value }) {
    this.setState({ isChecked: !!value });
  }

  _onPress = () => {
    const { isChecked } = this.state;
    const { onValueChange = noop } = this.props;

    this.setState({ isChecked: !isChecked });
    onValueChange(!isChecked);
  };

  render() {
    const { text = '', style, customStyle, checkboxStyle, lineHeight } = this.props;
    const { isChecked } = this.state;

    const source = isChecked ? IMAGES.checkBoxOn : IMAGES.checkBoxOff;

    return (
      <TouchableHighlight
        style={[styles.container, style]}
        underlayColor="transparent"
        onPress={this._onPress}
      >
        <View style={[styles.container, customStyle]}>
          <Image style={[styles.checkbox, checkboxStyle]} source={source} />
          {text ? <Text style={[styles.label, lineHeight]}>{text}</Text> : null}
        </View>
      </TouchableHighlight>
    );
  }
}

Component.propTypes = {
  customStyle: PropTypes.object,
  checkboxStyle: PropTypes.object,
  lineHeight: PropTypes.object,
  onValueChange: PropTypes.func,
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  text: PropTypes.string,
  value: PropTypes.bool.isRequired
};

Component.defaultProps = {
  customStyle: {},
  checkboxStyle: {},
  lineHeight: {},
  onValueChange: noop,
  style: {},
  text: ''
};
