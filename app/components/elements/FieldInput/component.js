import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import TextInput from '../TextInput';
import { isEmpty, noop } from '../../../utils';

export default class Component extends React.Component {
  state = {
    status: 'blurred',
    errorMessage: ''
  };

  _onBlur = text => {
    const { onBlur = noop } = this.props;
    this.setState({ status: 'blurred' });
    onBlur(text);
  };

  _onFocus = text => {
    const { onFocus = noop } = this.props;
    this.setState({ status: 'focused', errorMessage: '' });
    onFocus(text);
  };

  _onTextInputChanged = value => {
    const { onChangeInput = noop } = this.props;
    this.props.errorText = '';
    onChangeInput(value);
    this.setState({ errorMessage: '' });
  };

  _onValidation = value => {
    const { validate } = this.props;
    if (validate) {
      this.setState({ errorMessage: validate(value) });
    }
  };

  render() {
    const {
      label,
      floatingLabel = false,
      containerStyle,
      labelStyle,
      textStyle,
      placeholder = '',
      placeholderStyle,
      prefixInputText,
      defaultValue,
      prefixInputStyle,
      suffixInputText,
      textInputProps = {},
      errorTextStyle,
      onLayout = noop,
      setRef = noop,
      containerCustomStyle,
      customFocusedStyle,
      onSetTextInputRef = noop,
      onEnter = noop,
      errorText
    } = this.props;
    const { errorMessage, status } = this.state;
    const errorTemp = errorText || errorMessage;
    const labelTemp = floatingLabel && defaultValue.length === 0 ? '' : label;
    const textInputStyle = errorTemp !== '' ? styles.textInputError : styles.textInput;
    const labelStyleTemp = floatingLabel && status === 'focused' ? styles.labelFocus : styles.label;

    return (
      <View style={containerStyle} onLayout={onLayout} ref={setRef}>
        <Text style={[labelStyleTemp, labelStyle]}>{labelTemp}</Text>
        <View style={styles.inputContainer}>
          {!isEmpty(prefixInputText) && (
            <Text style={[styles.prefixText, prefixInputStyle]}>{prefixInputText}</Text>
          )}
          <View style={styles.textInputContainer}>
            <TextInput
              defaultValue={defaultValue}
              placeholderStyle={placeholderStyle}
              placeholder={placeholder}
              onChangeText={this._onTextInputChanged}
              textStyle={textStyle}
              containerCustomStyle={[textInputStyle, containerCustomStyle]}
              customFocusedStyle={[styles.textInputFocused, customFocusedStyle]}
              onValidation={this._onValidation}
              onBlur={this._onBlur}
              onFocus={this._onFocus}
              onSetRef={onSetTextInputRef}
              onEnter={onEnter}
              {...textInputProps}
            />
          </View>
          {!isEmpty(suffixInputText) && <Text style={styles.suffixText}>{suffixInputText}</Text>}
        </View>
        {(!isEmpty(errorText) || !isEmpty(errorMessage)) && (
          <Text style={[styles.errorText, errorTextStyle]}>{errorTemp}</Text>
        )}
      </View>
    );
  }
}

Component.displayName = 'FieldInput';

Component.propTypes = {
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onChangeInput: PropTypes.func.isRequired,
  errorText: PropTypes.string,
  label: PropTypes.string.isRequired,
  floatingLabel: PropTypes.bool,
  containerStyle: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  labelStyle: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  textStyle: PropTypes.object,
  placeholder: PropTypes.string,
  placeholderStyle: PropTypes.object,
  prefixInputText: PropTypes.string,
  prefixInputStyle: PropTypes.object,
  errorTextStyle: PropTypes.object,
  customFocusedStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onEnter: PropTypes.func,
  defaultValue: PropTypes.string.isRequired,
  suffixInputText: PropTypes.string,
  textInputProps: PropTypes.object,
  containerCustomStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onLayout: PropTypes.func,
  validate: PropTypes.func,
  onSetTextInputRef: PropTypes.func,
  setRef: PropTypes.func
};

Component.defaultProps = {
  containerCustomStyle: {},
  containerStyle: {},
  customFocusedStyle: {},
  errorText: '',
  errorTextStyle: {},
  floatingLabel: false,
  labelStyle: {},
  onBlur: noop,
  onFocus: noop,
  onEnter: noop,
  onLayout: noop,
  onSetTextInputRef: noop,
  placeholder: '',
  placeholderStyle: {},
  prefixInputStyle: {},
  prefixInputText: '',
  setRef: noop,
  suffixInputText: '',
  textInputProps: {},
  textStyle: {},
  validate: noop
};
