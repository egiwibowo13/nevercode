import { StyleSheet } from 'react-native';

import {
  COLOR_GREY,
  FONT_DISPLAY_MEDIUM,
  COLOR_GREEN,
  COLOR_GREY_STRONG,
  FONT_VALUE_MEDIUM,
  COLOR_ERROR,
  FONT_ERROR_MEDIUM
} from '../../../styles';

const textInput = {
  borderBottomWidth: 1,
  flex: 1
};

export default StyleSheet.create({
  label: {
    color: COLOR_GREY,
    ...FONT_DISPLAY_MEDIUM
  },
  labelFocus: {
    color: COLOR_GREEN,
    ...FONT_DISPLAY_MEDIUM
  },
  textInput: {
    ...textInput,
    borderColor: COLOR_GREY
  },
  textInputFocused: {
    ...textInput,
    borderColor: COLOR_GREEN
  },
  textInputError: {
    ...textInput,
    borderColor: COLOR_ERROR
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  textInputContainer: {
    flex: 1
  },
  prefixText: {
    ...FONT_VALUE_MEDIUM,
    color: COLOR_GREY_STRONG,
    marginRight: 12
  },
  suffixText: {
    ...FONT_VALUE_MEDIUM,
    color: COLOR_GREY_STRONG,
    marginLeft: 12
  },
  errorText: {
    ...FONT_ERROR_MEDIUM,
    color: COLOR_ERROR
  }
});
