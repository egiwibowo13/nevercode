import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { IMAGES } from '../../../configs';

export default class Component extends React.Component {
  render() {
    const { onPressCamera, onPressGaleri, onCloseModal } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.modal}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={styles.title}>selectFrom</Text>
            <TouchableOpacity onPress={onCloseModal}>
              <Image
                style={{ width: 14, height: 14, marginRight: 23, marginTop: 5 }}
                source={IMAGES.userOn}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.selection} onPress={onPressCamera}>
            <Image style={styles.img} source={IMAGES.userOn} />
            <Text style={styles.menus}>camera</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selection} onPress={onPressGaleri}>
            <Image style={styles.img} source={IMAGES.userOn} />
            <Text style={styles.menus}>gallery</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

Component.propTypes = {
  onPressCamera: PropTypes.func.isRequired,
  onPressGaleri: PropTypes.func.isRequired,
  onCloseModal: PropTypes.func.isRequired
};
