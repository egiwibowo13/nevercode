import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import { IMAGES } from '../../../configs';
import styles from './styles';

export default class Component extends React.Component {
  render() {
    const { onPress, customSource, customStyle } = this.props;
    const images = customSource || IMAGES.back;

    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image source={images} style={[styles.image, customStyle]} />
      </TouchableOpacity>
    );
  }
}

Component.propTypes = {
  onPress: PropTypes.func.isRequired,
  customSource: PropTypes.object,
  customStyle: PropTypes.object
};

Component.defaultProps = {
  customSource: undefined,
  customStyle: {}
};
