import { StyleSheet } from 'react-native';
import { COLOR_GREEN } from '../../../styles';

export default StyleSheet.create({
  container: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    tintColor: COLOR_GREEN,
    width: 16,
    height: 16
  }
});
