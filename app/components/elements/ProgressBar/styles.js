import { StyleSheet } from 'react-native';

import { COLOR_WHITE_OPACITY70 } from '../../../styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR_WHITE_OPACITY70
  }
});

export default styles;
