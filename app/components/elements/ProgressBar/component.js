import React, { Component } from 'react';
import { Platform, View, ProgressBarAndroid, ProgressViewIOS } from 'react-native';
import styles from './styles';

export default class MyApp extends Component {
  constructor() {
    super();
    this.state = {
      progrss: 0.0,
      invert: false
    };
  }

  componentDidMount() {
    this._startProgress();
  }

  _startProgress = async () => {
    this.value = await setInterval(async () => {
      if (this.state.progrss <= 0) {
        await this.setState({ invert: false, progrss: this.state.progrss + 0.1 });
      } else if (this.state.progrss > 0 && this.state.progrss < 1) {
        if (this.state.invert) {
          await this.setState({ progrss: this.state.progrss - 0.1 });
        } else {
          await this.setState({ progrss: this.state.progrss + 0.1 });
        }
      } else if (this.state.progrss >= 1) {
        await this.setState({ invert: true, progrss: this.state.progrss - 0.1 });
      }
    }, 100);
  };

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === 'android' ? (
          <ProgressBarAndroid
            styleAttr="Horizontal"
            progress={this.state.progrss}
            indeterminate={false}
            color="red"
          />
        ) : (
          <ProgressViewIOS progress={this.state.progrss} />
        )}
      </View>
    );
  }
}
