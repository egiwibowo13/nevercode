import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { noop } from '../../../utils';

export default class Component extends React.Component {
  _renderRadioButtonCircle = () => {
    const { selected = false, circleStyle } = this.props;
    return <View style={[styles.circle, circleStyle]}>{selected && <View style={styles.selected} />}</View>;
  };

  _renderRadioButtonLabel = () => {
    const { name = '', labelStyle } = this.props;
    return <Text style={[styles.label, labelStyle]}>{name}</Text>;
  };

  _renderRadioButton = () => {
    const { containerStyle, shiftedLabel = true } = this.props;

    return shiftedLabel ? (
      <View style={[styles.container, containerStyle]}>
        {this._renderRadioButtonCircle()}
        {this._renderRadioButtonLabel()}
      </View>
    ) : (
      <View style={[styles.container, containerStyle]}>
        {this._renderRadioButtonLabel()}
        {this._renderRadioButtonCircle()}
      </View>
    );
  };

  _onSelect = () => {
    const { onSelect = noop, index, name, value } = this.props;
    const selectingName = name;
    onSelect(index, selectingName, value);
  };

  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={this._onSelect} activeOpacity={1}>
        {this._renderRadioButton()}
      </TouchableOpacity>
    );
  }
}

Component.propTypes = {
  labelStyle: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  circleStyle: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  selected: PropTypes.bool.isRequired,
  containerStyle: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  onSelect: PropTypes.func.isRequired,
  index: PropTypes.number,
  shiftedLabel: PropTypes.bool
};

Component.defaultProps = {
  labelStyle: {},
  circleStyle: {},
  containerStyle: {},
  index: 0,
  shiftedLabel: true
};
