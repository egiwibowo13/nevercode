import { StyleSheet } from 'react-native';
import { COLOR_GREEN, COLOR_GREY_EXTRA_STRONG, COLOR_WHITE, FONT_DROPDOWN_REGULAR } from '../../../styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  circle: {
    width: 20,
    height: 20,
    margin: 10,
    borderWidth: 2,
    borderColor: COLOR_GREEN,
    borderRadius: 10,
    alignSelf: 'flex-start',
    justifyContent: 'center'
  },
  label: {
    ...FONT_DROPDOWN_REGULAR,
    color: COLOR_GREY_EXTRA_STRONG,
    alignSelf: 'center',
    textAlign: 'left',
    textAlignVertical: 'center'
  },
  selected: {
    width: 16,
    height: 16,
    borderRadius: 8,
    borderWidth: 1.5,
    borderColor: COLOR_WHITE,
    backgroundColor: COLOR_GREEN
  }
});
