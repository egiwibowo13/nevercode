import React from 'react';
import { View, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import RadioButton from '../RadioButton';
import styles from './styles';
import { noop } from '../../../utils';

export default class Component extends React.Component {
  render() {
    const {
      options = [],
      selected = '',
      spaceBetween = 0,
      numColumns = 1,
      onSelect = noop,
      disabledLeft
    } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          numColumns={numColumns}
          data={options}
          extraData={this.props}
          renderItem={({ item, index }) => {
            const selectedTemp = selected === item.name;

            return (
              <RadioButton
                containerStyle={index !== 0 && { marginLeft: spaceBetween }}
                key={item.id}
                index={index}
                name={item.name}
                value={item.id}
                selected={selectedTemp}
                onSelect={onSelect}
                labelStyle={disabledLeft && index === 0 && styles.labelStyle}
                circleStyle={disabledLeft && index === 0 && styles.circleStyle}
              />
            );
          }}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

Component.propTypes = {
  options: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  numColumns: PropTypes.number,
  onSelect: PropTypes.func.isRequired,
  disabledLeft: PropTypes.bool,
  spaceBetween: PropTypes.number
};

Component.defaultProps = {
  numColumns: 1,
  disabledLeft: false,
  spaceBetween: 2
};
