import { StyleSheet } from 'react-native';
import {
  FONT_DISPLAY_REGULAR,
  COLOR_GREY_STRONG,
  COLOR_GREY_EXTRA_STRONG,
  COLOR_BLACK_OPACITY50,
  FONT_VALUE_MEDIUM,
  COLOR_GREY_LIGHT
} from '../../../styles';
import { widthByScreen } from '../../../utils/dimensions';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: COLOR_BLACK_OPACITY50
  },
  modal: {
    position: 'absolute',
    width: widthByScreen(100),
    bottom: 0,
    backgroundColor: COLOR_GREY_LIGHT
  },
  title: {
    ...FONT_DISPLAY_REGULAR,
    paddingVertical: 10,
    textAlign: 'left',
    color: COLOR_GREY_STRONG,
    alignSelf: 'stretch',
    marginLeft: 20
  },
  menus: {
    ...FONT_VALUE_MEDIUM,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginLeft: 20,
    color: COLOR_GREY_EXTRA_STRONG,
    alignItems: 'center',
    justifyContent: 'center'
  },
  selection: {
    paddingVertical: 12,
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 20
  },
  cancel: {
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  img: {
    height: 25,
    width: 25,
    alignSelf: 'center'
  }
});

export default styles;
