import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  DatePickerIOS,
  DatePickerAndroid,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { IMAGES } from '../../../configs';
import I18n from '../../../i18n';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._onDateChange = this._onDateChange.bind(this);
  }

  componentWillReceiveProps = async nextProps => {
    if (nextProps.visible && Platform.OS === 'android') {
      this._onOpenDatePicker();
    }
  };

  _onOpenDatePicker = async () => {
    const { date, onDateChange, onCloseModal } = this.props;
    const dateValue = date ? new Date(date) : new Date(1960, 1, 1);
    await DatePickerAndroid.open({
      date: dateValue,
      maxDate: new Date(),
      mode: 'default'
    }).then(async pickedDate => {
      const { year, month, day, action } = pickedDate;
      if (action !== 'dateSetAction') return;
      const datePicker = new Date(Date.UTC(year, month, day)).toISOString();
      await onDateChange(datePicker);
      await onCloseModal();
    });
  };

  _onDateChange = async newDate => {
    const { onDateChange } = this.props;
    onDateChange(newDate);
  };

  render() {
    const { onCloseModal, customStyle, date, visible } = this.props;
    const dateValue = date ? new Date(date) : new Date(1960, 1, 1);
    if (Platform.OS === 'android') return null;
    return (
      <Modal visible={visible} transparent>
        <TouchableWithoutFeedback onPress={onCloseModal}>
          <View style={styles.container} />
        </TouchableWithoutFeedback>
        <View style={[styles.modal, customStyle]}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={styles.title}>{I18n.t('selectDate')}</Text>
            <TouchableOpacity onPress={onCloseModal}>
              <Image
                style={{ width: 14, height: 14, marginRight: 23, marginTop: 5 }}
                source={IMAGES.userOn}
              />
            </TouchableOpacity>
          </View>
          <DatePickerIOS mode="date" date={dateValue} onDateChange={this._onDateChange} />
        </View>
      </Modal>
    );
  }
}

Component.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  customStyle: PropTypes.object,
  date: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired
};

Component.defaultProps = {
  customStyle: {}
};
