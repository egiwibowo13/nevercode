import React from 'react';
import { Image, View, Text } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { noop } from '../../../utils';
import styles from './styles';
import { IMAGES } from '../../../configs';

export default class Component extends React.Component {
  state = {
    isOpen: false,
    itemSelected: {},
    dropdownWidth: 30
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultId !== this.props.defaultId) {
      this.setState({ itemSelected: {} });
    }
  }

  _onDropdownWillShow = () => {
    this.setState({
      isOpen: true
    });
  };

  _onDropdownWillHide = () => {
    this.setState({
      isOpen: false
    });
  };

  _onSelectItem = (index, item) => {
    this.setState({
      itemSelected: item,
      isOpen: false
    });
    const { onItemSelected = noop } = this.props;
    onItemSelected(item);
  };

  _renderItem = rowData => {
    const { itemSelected = {} } = this.state;
    const itemStyle = rowData.id === itemSelected.id ? styles.activeItem : styles.item;
    return (
      <View key={rowData.id} style={styles.itemRow}>
        <Text style={itemStyle}>{rowData.name}</Text>
      </View>
    );
  };

  _dropdownAdjustFrame = style => ({ ...style, width: this.state.dropdownWidth });

  _onLayout = e => {
    this.setState({ dropdownWidth: e.nativeEvent.layout.width });
  };

  _getNameById = name => {
    const { options = [] } = this.props;
    const foundOption = options.find(option => option.name === name) || {};
    return foundOption.name;
  };

  render() {
    const { isOpen, itemSelected } = this.state;
    const { options = [], placeholder, defaultId } = this.props;
    const styleIcon = isOpen ? styles.iconRotate : styles.icon;
    const dropdownValue = !isEmpty(itemSelected) ? itemSelected.name : this._getNameById(defaultId);
    const hasItemSelected = !isEmpty(dropdownValue);

    const displayText = hasItemSelected ? dropdownValue : placeholder;

    const itemSelectedStyle = hasItemSelected ? [styles.activeItem] : [styles.item, styles.placeholderText];

    return (
      <ModalDropdown
        options={options}
        onSelect={this._onSelectItem}
        renderRow={this._renderItem}
        onDropdownWillShow={this._onDropdownWillShow}
        onDropdownWillHide={this._onDropdownWillHide}
        dropdownStyle={styles.dropdownStyle}
        dropdownShowInTopStyle={styles.dropdownShowInTopStyle}
        renderSeparator={() => false}
        adjustFrame={this._dropdownAdjustFrame}
        animated={false}
      >
        {showInBottom => (
          <View
            onLayout={this._onLayout}
            style={
              isOpen
                ? [styles.dropdownButton, !showInBottom && styles.dropdownButtonShowInTop]
                : styles.dropdown
            }
          >
            <Text style={itemSelectedStyle}>{displayText}</Text>
            <Image style={styleIcon} source={IMAGES.dropdown} />
          </View>
        )}
      </ModalDropdown>
    );
  }
}

Component.propTypes = {
  defaultId: PropTypes.string.isRequired,
  onItemSelected: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  placeholder: PropTypes.string.isRequired
};
