import { assertSnapshots } from '../../../../utils/TestUtils/snapshot';
import Component from '../index';

jest.mock('../styles', () => ({}));
jest.mock('TouchableOpacity', () => 'TouchableOpacity');
jest.mock('Text', () => 'Text');
jest.mock('FlatList', () => 'FlatList');
jest.mock('Image', () => 'Image');

const data = [
  {
    name: 'A',
    id: '1'
  },
  {
    name: 'B',
    id: '2'
  }
];

describe('Component snapshot testing', () => {
  const configs = [
    {
      props: {
        options: data
      },
      desc: 'renders with data'
    },
    {
      props: {
        options: []
      },
      desc: 'renders empty component'
    },
    {
      props: {
        options: data
      },
      state: { isOpen: true },
      desc: 'Dropdown is open'
    },
    {
      props: {
        options: data
      },
      state: { isOpen: true, itemSelected: { id: '1', name: 'name' } },
      desc: 'Dropdown is render with itemSelected'
    },
    {
      props: {},
      desc: 'renders without data'
    }
  ];
  assertSnapshots(Component, configs);
});
