import Component from '../index';
import { mockSetState } from '../../../../utils/TestUtils/snapshot';
import { noop } from '../../../../utils';

jest.mock('../../../../utils', () => ({
  noop: jest.fn()
}));

describe('Component', () => {
  const ComponentWithMockedState = mockSetState(Component);
  const getInstance = props => new ComponentWithMockedState(props);
  beforeEach(() => {
    this.props = {
      options: [
        {
          name: 'A',
          id: '1'
        },
        {
          name: 'B',
          id: '2'
        }
      ],
      onItemSelected: jest.fn()
    };
  });

  afterEach(() => {
    this.props = null;
    jest.clearAllMocks();
  });

  test('componentWillReceiveProps defaultId undefined', () => {
    this.props.defaultId = undefined;
    const instance = getInstance(this.props);
    const value = {
      defaultId: 'defaultId'
    };
    instance.componentWillReceiveProps(value);
    expect(instance.state.itemSelected).toEqual({});
  });

  test('componentWillReceiveProps have defaultId', () => {
    this.props.defaultId = 'defaultId';
    const instance = getInstance(this.props);
    const value = {
      defaultId: 'defaultId'
    };
    instance.componentWillReceiveProps(value);
    expect(instance.state.itemSelected).toEqual({});
  });

  test('should set state when _onDropdownWillShow', () => {
    const instance = getInstance(this.props);
    instance._onDropdownWillShow();
    expect(instance.state.isOpen).toBe(true);
  });

  test('should set state when _onDropdownWillHide', () => {
    const instance = getInstance(this.props);
    instance._onDropdownWillHide();
    expect(instance.state.isOpen).toBe(false);
  });

  test('should call onItemSelected when click on item', () => {
    const instance = getInstance(this.props);
    instance._onSelectItem();
    expect(this.props.onItemSelected).toHaveBeenCalled();
  });

  test('_onSelectItem when onItemSelected undefined', () => {
    this.props.onItemSelected = undefined;
    const instance = getInstance(this.props);
    instance._onSelectItem();
    expect(noop).toHaveBeenCalled();
  });

  test('should call _dropdownAdjustFrame when click on item', () => {
    const instance = getInstance(this.props);
    const result = instance._dropdownAdjustFrame({});
    const expectResult = {
      width: 30
    };
    expect(result).toEqual(expectResult);
  });

  test('_onLayout', () => {
    const instance = getInstance(this.props);
    const width = 100;
    const e = { nativeEvent: { layout: { width } } };
    instance._onLayout(e);
    expect(instance.state.dropdownWidth).toBe(width);
  });

  test('_renderItem', () => {
    const instance = getInstance(this.props);
    instance.setState({
      itemSelected: { id: 'id' }
    });
    const rowData = { id: 'id' };
    instance._renderItem(rowData);
  });

  test('_renderItem itemSelected undefined', () => {
    const instance = getInstance(this.props);
    instance.setState({
      itemSelected: undefined
    });
    const rowData = { id: 'id' };
    instance._renderItem(rowData);
  });
});
