import { StyleSheet } from 'react-native';

import {
  FONT_DROPDOWN_REGULAR,
  COLOR_LIGHT,
  COLOR_GREY,
  COLOR_GREY_STRONG,
  COLOR_GREY_EXTRA_STRONG,
  FONT_VALUE_MEDIUM,
  FONT_H2
} from '../../../styles';

const font = {
  ...FONT_DROPDOWN_REGULAR
};

const iconStyle = {
  height: 16,
  width: 16,
  alignItems: 'center',
  justifyContent: 'center',
  marginRight: 16
};

const dropdownStyle = {
  backgroundColor: COLOR_LIGHT,
  flexDirection: 'row',
  paddingVertical: 4,
  justifyContent: 'space-between',
  alignItems: 'center'
};

export default StyleSheet.create({
  container: {
    borderColor: COLOR_GREY,
    borderWidth: 1,
    borderRadius: 4
  },
  dropdown: {
    borderColor: COLOR_GREY,
    borderWidth: 1,
    borderRadius: 4,
    ...dropdownStyle
  },
  dropdownButton: {
    borderColor: COLOR_GREY,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    ...dropdownStyle
  },
  dropdownButtonShowInTop: {
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4
  },
  text: {
    flex: 1,
    ...font,
    color: COLOR_GREY_STRONG
  },
  activedText: {
    flex: 1,
    ...FONT_VALUE_MEDIUM,
    color: COLOR_GREY_EXTRA_STRONG
  },
  icon: {
    ...iconStyle
  },
  iconRotate: {
    transform: [{ rotate: '180deg' }],
    ...iconStyle
  },
  item: {
    ...FONT_VALUE_MEDIUM,
    color: COLOR_GREY_EXTRA_STRONG,
    flex: 1,
    paddingVertical: 5,
    backgroundColor: COLOR_LIGHT,
    paddingLeft: 16
  },
  activeItem: {
    ...FONT_H2,
    color: COLOR_GREY_EXTRA_STRONG,
    flex: 1,
    paddingVertical: 5,
    paddingLeft: 16
  },
  dropdownStyle: {
    borderColor: COLOR_GREY,
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    backgroundColor: COLOR_LIGHT
  },
  dropdownShowInTopStyle: {
    borderTopWidth: 1,
    borderBottomWidth: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4
  },
  placeholderText: {
    color: COLOR_GREY
  },
  itemRow: {
    backgroundColor: COLOR_LIGHT
  }
});
