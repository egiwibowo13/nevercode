import React from 'react';
import { Image, TextInput, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { COLOR_GREEN, COLOR_GREY } from '../../../styles';
import { isEmpty, noop } from '../../../utils';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: 'blurred',
      text: props.value || props.defaultValue
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ text: nextProps.value });
    }
  }

  _onBlur = () => {
    const { text } = this.state;
    this.setState({ status: 'blurred' });

    const { onBlur = noop, onValidation = noop } = this.props;
    onBlur(text);
    onValidation(text);
  };

  _onFocus = () => {
    const { text } = this.state;
    this.setState({ status: 'focused' });

    const { onFocus = noop } = this.props;
    onFocus(text);
  };

  _onChangeText = text => {
    const { onChangeText = noop } = this.props;
    this.setState({ text });

    onChangeText(text);
  };

  _getIcon() {
    const { highlightedIcon, normalIcon, isValid = true, defaultValue } = this.props;

    const text = this.state.text || defaultValue;
    let icon = normalIcon;

    // Select style and icon based on validity and focus / blur status
    if (!isEmpty(text) && isValid) {
      icon = highlightedIcon;
    }

    return icon;
  }

  _renderIcon() {
    const { iconEnabled } = this.props;
    const icon = this._getIcon();

    return iconEnabled ? (
      <Image style={styles.icon} resizeMode="contain" source={icon} />
    ) : (
      <View style={styles.icon} />
    );
  }

  render() {
    const {
      isValid = true,
      containerCustomStyle,
      customFocusedStyle,
      textStyle,
      onSetRef = noop,
      onEnter = noop
    } = this.props;
    const icon = this._getIcon();
    let containerStyle = {};
    const containerInputStyle = containerCustomStyle || styles.container;
    const containerFocusedStyle = customFocusedStyle || styles.containerFocused;

    if (isValid) {
      containerStyle = this.state.status === 'blurred' ? containerInputStyle : containerFocusedStyle;
    } else {
      containerStyle = styles.containerInvalid;
    }

    const textInputStyle = this.state.status === 'blurred' ? styles.input : styles.inputFocused;
    return (
      <View style={styles.wrapper}>
        {icon && this._renderIcon()}
        <View style={containerStyle}>
          <TextInput
            {...this.props}
            ref={onSetRef}
            style={[textInputStyle, textStyle]}
            placeholderTextColor={COLOR_GREY}
            selectionColor={COLOR_GREEN}
            underlineColorAndroid="transparent"
            onChangeText={this._onChangeText}
            onBlur={this._onBlur}
            onFocus={this._onFocus}
            onSubmitEditing={onEnter}
          />
        </View>
      </View>
    );
  }
}

Component.propTypes = {
  isValid: PropTypes.bool,
  iconEnabled: PropTypes.bool,
  containerCustomStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  customFocusedStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  defaultValue: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onValidation: PropTypes.func,
  onChangeText: PropTypes.func.isRequired,
  highlightedIcon: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  normalIcon: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  textStyle: PropTypes.object,
  value: PropTypes.string.isRequired,
  onSetRef: PropTypes.func,
  onEnter: PropTypes.func
};

Component.defaultProps = {
  containerCustomStyle: {},
  customFocusedStyle: {},
  highlightedIcon: {},
  normalIcon: {},
  iconEnabled: false,
  isValid: true,
  onBlur: noop,
  onFocus: noop,
  onEnter: noop,
  onValidation: noop,
  onSetRef: noop,
  style: {},
  textStyle: {}
};
