import { StyleSheet } from 'react-native';

import { COLOR_GREY_EXTRA_STRONG, COLOR_GREEN, COLOR_GREY, COLOR_ERROR } from '../../../styles';

const input = {
  color: COLOR_GREY_EXTRA_STRONG,
  paddingHorizontal: 0,
  paddingVertical: 5
};

const container = {
  borderBottomWidth: 1,
  flex: 1,
  marginLeft: 14
};

export default StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  container: {
    borderColor: COLOR_GREY,
    ...container
  },
  containerFocused: {
    borderColor: COLOR_GREEN,
    ...container
  },
  containerInvalid: {
    borderColor: COLOR_ERROR,
    ...container
  },
  icon: {
    width: 16,
    height: 16,
    marginRight: 10,
    marginTop: 5
  },
  input: {
    ...input
  },
  inputFocused: {
    ...input
  }
});
