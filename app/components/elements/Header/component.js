import React from 'react';
import { StatusBar, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import BackButton from '../BackButton';
import { noop } from '../../../utils';
import SearchTextInput from '../SearchTextInput';
import SearchButton from '../SearchButton';
import { COLOR_LIGHT } from '../../../styles';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = { searching: false };
    StatusBar.setBackgroundColor(COLOR_LIGHT);
    StatusBar.setBarStyle('dark-content');
    StatusBar.animated = true;
  }

  _onPressSearch = () => {
    this.setState({ searching: true });
  };

  _onCancelSearch = () => {
    const { onCancelSearch = noop } = this.props;
    this.setState({ searching: false });
    onCancelSearch();
  };

  _renderSearchTextInput = () => {
    const { onChangeSearchText = noop, placeholderSearchText } = this.props;
    return (
      <SearchTextInput
        onCancel={this._onCancelSearch}
        onChangeText={onChangeSearchText}
        containerStyle={styles.textInputContainerStyle}
        placeholder={placeholderSearchText}
      />
    );
  };

  render() {
    const { title, subTitle = '', onPressBack = noop, showSearchButton = false, rightView } = this.props;
    const { searching } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          {!searching && <BackButton onPress={onPressBack} />}
          {rightView}
          {showSearchButton && !searching && <SearchButton onPress={this._onPressSearch} />}
          {searching && this._renderSearchTextInput()}
        </View>
        <Text style={styles.containerTitle}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subTitle}>{`   ${subTitle}`}</Text>
        </Text>
      </View>
    );
  }
}

Component.propTypes = {
  onPressBack: PropTypes.func.isRequired,
  onCancelSearch: PropTypes.func,
  onChangeSearchText: PropTypes.func,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  showSearchButton: PropTypes.bool,
  rightView: PropTypes.element,
  placeholderSearchText: PropTypes.string
};

Component.defaultProps = {
  onCancelSearch: noop,
  onChangeSearchText: noop,
  title: '',
  subTitle: '',
  showSearchButton: false,
  placeholderSearchText: '',
  rightView: <View />
};
