import React from 'react';
import { View, TextInput, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { IMAGES } from '../../../configs';
import { noop } from '../../../utils';
import { COLOR_GREY } from '../../../styles';

export default class Component extends React.Component {
  render() {
    const {
      containerStyle,
      textInputStyle,
      searchImageStyle,
      closeImageStyle,
      placeholder,
      placeholderTextColor = COLOR_GREY,
      onChangeText = noop,
      onCancel = noop
    } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <Image source={IMAGES.search} style={[styles.image, searchImageStyle]} />
        <TextInput
          style={[styles.textInput, textInputStyle]}
          underlineColorAndroid="transparent"
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          onChangeText={onChangeText}
          autoFocus
        />
        <TouchableOpacity onPress={onCancel} style={styles.cancelButtonContainer}>
          <Image source={IMAGES.close} style={[styles.closeImage, closeImageStyle]} />
        </TouchableOpacity>
      </View>
    );
  }
}

Component.propTypes = {
  containerStyle: PropTypes.object,
  textInputStyle: PropTypes.object,
  searchImageStyle: PropTypes.object,
  closeImageStyle: PropTypes.object,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

Component.defaultProps = {
  containerStyle: {},
  textInputStyle: {},
  searchImageStyle: {},
  closeImageStyle: {},
  placeholder: '',
  placeholderTextColor: COLOR_GREY
};
