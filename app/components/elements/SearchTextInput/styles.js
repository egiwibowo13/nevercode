import { StyleSheet } from 'react-native';
import { COLOR_GREY_EXTRA_STRONG, COLOR_GREY_STRONG, FONT_LABEL_MEDIUM, COLOR_GREY } from '../../../styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: COLOR_GREY,
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 4,
    height: 44
  },
  textInput: {
    flex: 1,
    color: COLOR_GREY_EXTRA_STRONG,
    ...FONT_LABEL_MEDIUM
  },
  image: {
    tintColor: COLOR_GREY_STRONG,
    width: 32,
    height: 32
  },
  closeImage: {
    width: 14,
    height: 14,
    tintColor: COLOR_GREY
  },
  cancelButtonContainer: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
