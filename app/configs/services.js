// export const services = {
//     BASE_URL: 'https://us-central1-codebase-bbb16.cloudfunctions.net/',
//     TERMINAL: '...',
//     API_KEY: '...',
//     HEADER_CONTENT_TYPE: 'application/json',
//     HEADER_AUTH: '...',
//   };
import { get, post } from './networking';

// end point
export const endpoint = {
  getListUser: async page => get(`/api/users?page=${page}`),
  getUserById: async id => get(`/api/users/${id}`),
  login: async params => post('/api/login', params)
};

export default { endpoint };
