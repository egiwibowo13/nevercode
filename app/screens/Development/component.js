import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MainScreen from '../../components/layouts/MainScreen';

export default class Component extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <MainScreen navigation={navigation} title="Development" showSearchButton>
        <View style={styles.container}>
          <Text>Development</Text>
        </View>
      </MainScreen>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
