import { TYPES } from '../../constants';

const initialState = {
  listUsers: [],
  isLoading: false,
  error: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.GET_LIST_USERS:
      return {
        ...state,
        isLoading: true,
        listUsers: []
      };
    case TYPES.GET_LIST_USERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listUsers: action.data
      };
    case TYPES.GET_LIST_USERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    default:
      return state;
  }
}
