/* eslint no-use-before-define: ["error", { "functions": false }] */
import { TYPES } from '../../constants';
import { ENDPOINT } from '../../configs';

export function fetchGetListUser() {
  return dispatch => {
    dispatch(getListUsers());
    ENDPOINT.getListUser('1')
      .then(data => {
        dispatch(getListUsersSuccess(data));
      })
      .catch(err => dispatch(getListUsersFailure(err)));
  };
}

function getListUsers() {
  return {
    type: TYPES.GET_LIST_USERS
  };
}

function getListUsersSuccess(data) {
  return {
    type: TYPES.GET_LIST_USERS_SUCCESS,
    data
  };
}

function getListUsersFailure(err) {
  return {
    type: TYPES.GET_LIST_USERS_FAILURE,
    err
  };
}

export default fetchGetListUser;
