import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import FormLogin from '../../components/forms/FormLogin';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _onFormChange = async (key, value) => {
    await this.setState({ [key]: value });
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <FormLogin navigation={navigation} />
      </View>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
