import { StyleSheet } from 'react-native';
import { PRIMARY_COLOR, COLOR_WHITE } from '../../styles';

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: PRIMARY_COLOR
  },
  container: {
    flex: 1,
    backgroundColor: COLOR_WHITE,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 20
  },
  radioContainer: {
    width: '100%',
    height: 24,
    marginTop: 8
  }
});

export default styles;
