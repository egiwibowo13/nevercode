import React from 'react';
import { Dimensions } from 'react-native';
import AppIntro from 'react-native-app-intro';
import PropTypes from 'prop-types';
import { IMAGES } from '../../configs';

const WIDTH_LAYOUT = Dimensions.get('window').width;

export default class Component extends React.Component {
  // onSkipBtnHandle = () => {
  //   Alert.alert('Skip');
  // };
  // doneBtnHandle = () => {
  //   Alert.alert('Done');
  // };
  // nextBtnHandle = () => {
  //   Alert.alert('Next');
  // };
  // onSlideChangeHandle = (index, total) => {
  //   console.log(index, total);
  // }

  _onPress = () => {
    const { navigation } = this.props;
    navigation.navigate('Login');
  };

  render() {
    const pageArray = [
      {
        title: 'Page 1',
        description: 'Description 1',
        img: IMAGES.onBoarding[0],
        imgStyle: {
          height: 100 * 2.5,
          width: WIDTH_LAYOUT
        },
        backgroundColor: '#EADD5A',
        fontColor: '#fff',
        level: 10
      },
      {
        title: 'Page 2',
        description: 'Description 2',
        img: IMAGES.onBoarding[1],
        imgStyle: {
          height: 100 * 2.5,
          width: WIDTH_LAYOUT
        },
        backgroundColor: '#F8E070',
        fontColor: '#fff',
        level: 10
      },
      {
        title: 'Page 3',
        description: 'Description 2',
        img: IMAGES.onBoarding[2],
        imgStyle: {
          height: 100 * 2.5,
          width: WIDTH_LAYOUT
        },
        backgroundColor: '#FCEE4F',
        fontColor: '#fff',
        level: 10
      }
    ];
    return (
      <AppIntro
        // onNextBtnClick={this.nextBtnHandle}
        onDoneBtnClick={this._onPress}
        onSkipBtnClick={this._onPress}
        //  onSlideChange={this.onSlideChangeHandle}
        pageArray={pageArray}
        showSkipButton
        showDoneButton
        showDots
      />
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
